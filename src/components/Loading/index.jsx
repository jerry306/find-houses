import { useEffect } from "react";
import { Toast } from "antd-mobile";
const Loading = () => {
  useEffect(() => {
    Toast.loading("Loading...", 0, null, false);
    return () => {
      Toast.hide();
    };
  }, []);
  return null;
};

export default Loading;
