import React, { useState } from "react";
import { PickerView } from "antd-mobile";
import FilterFooter from "../../../../components/FilterFooter";
export default function FilterPicker(props) {
  const [value, setvalue] = useState(props.ddefaultValue);
  const { onCancel, onSave, data, cols, type } = props;
  return (
    <>
      <PickerView
        data={data}
        value={value}
        cols={cols}
        onChange={(value) => setvalue(value)}
      />
      <FilterFooter
        onCancel={() => onCancel(type)}
        onOk={() => onSave(type, value)}
      />
    </>
  );
}
